# PHP-MySQL-pdf-File-upload-and-extract-info-text-and-meta-data

> This code example demonstrates the process to implement the  pdf file upload functionality in web applications and the following functionality will be implemented.

- [x] HTML form to upload pdf files.
- [x] Store file name  and  size and meta data and text of pages in the database using PHP and MySQL.
- [x] display the info of pdf files  in the web page.
